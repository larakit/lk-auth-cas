<?php
Route::group(['middleware' => 'web'], function () {
    Route::get('login', function () {
        if (env('CAS_SKIP')) {
            $user = \App\User::first();
            Auth::login($user, true);

            return Redirect::to('/admin');
        }

        \Larakit\Auth\LkCas::init();
        $attrs = \phpCAS::getAttributes();
        /** @var \App\User $user */
        $email     = \Illuminate\Support\Arr::get($attrs, 'jabber');
        $person_id = \Illuminate\Support\Arr::get($attrs, 'icPersonId');
        $user      = \App\User::where('login', '=', \phpCAS::getUser())
                              ->first();
        if (!$user && $email) {
            $user = \App\User::where('email', '=', $email)
                             ->first();
        }
        if (!$user && $person_id) {
            $user = \App\User::where('person_id', '=', $person_id)
                             ->first();
        }
        if (!$user) {
            $user = new \App\User();
        }
        $user->person_id = $person_id;
        $user->name      = \Illuminate\Support\Arr::get($attrs, 'name');
        $user->city      = \Illuminate\Support\Arr::get($attrs, 'place');
        $user->login     = \phpCAS::getUser();
        $user->email     = $email;
        $user->save();
        $user->touch();
        if (\Auth::guest()) {
            \Auth::login($user, true);
        }
        $callback = config('larakit.lk-auth-cas.callback_login');
        if (is_callable($callback)) {
            $callback();
        }
        $url = env('AUTH_CAS_AFTER_LOGIN_URL', '/?after_auth');

        return Redirect::to($url);
    })
         ->name('login');
    Route::get('logout', function () {
        Auth::logout();
        Session::put('cas_logout', true);

        if (Request::ajax()) {
            return [
                'result'  => 'success',
                'message' => 'Вы успешно вышли',
            ];
        } else {
            return Redirect::to('/logout-cas?123');
        }
    })
         ->name('logout');
    Route::get('logout-cas', function () {
        \Larakit\Auth\LkCas::init();
        \phpCAS::handleLogoutRequests(true, config('larakit.lk-auth-cas.allowed_clients'));
        \phpCAS::logout();
        Session::forget('cas_logout');

        return Redirect::to('/?logout_cas');
    });
});
