<?php

namespace Larakit\Auth;

class LkCas {
    
    static $is_init = false;
    
    static function init() {
        $log = config('larakit.lk-auth-cas.log_file');
        if($log) {
            \phpCAS::setDebug(storage_path($log));
        }
        if(!self::$is_init) {
            \phpCAS::client(
                CAS_VERSION_2_0,
                config('larakit.lk-auth-cas.server_hostname'),
                config('larakit.lk-auth-cas.server_port'),
                config('larakit.lk-auth-cas.server_uri'));
            \phpCAS::setNoCasServerValidation();
            \phpCAS::forceAuthentication();
            self::$is_init = true;
        }
    }
}