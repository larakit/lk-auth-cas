<?php

namespace Larakit\Auth;

use Closure;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class MiddlewareCAS {
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if(!(bool) env('skip_auth')) {
            \Larakit\Auth\LkCas::init();
            $attrs = \phpCAS::getAttributes();
            $roles = (string) Arr::get($attrs, 'roles', '');
            $roles = mb_strtolower($roles);
            $callback_reject = config('larakit.lk-auth-cas.callback_reject');
    
            //проверяем роли, обязательные для посещения страницы
            $roles_required = (array) config('larakit.lk-auth-cas.roles_required', []);
            if($roles_required) {
                foreach($roles_required as $role) {
                    if(!Str::is('*' . mb_strtolower($role) . '*', $roles)) {
                        $callback_reject();
                    }
                }
            }
            //проверяем роли, запрещенные для посещения страницы
            $roles_disabled = (array) config('larakit.lk-auth-cas.roles_disabled', []);
            if($roles_disabled) {
                foreach($roles_disabled as $role) {
                    if(Str::is('*' . mb_strtolower($role) . '*', $roles)) {
                        $callback_reject();
                    }
                }
            }
        }
        
        return $next($request);
    }
}