<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 10.08.17
 * Time: 17:26
 */

namespace Larakit\Auth;

class UserObserverCas {
    
    public function saving($model) {
        $model->city      = (string) $model->city;
        $model->person_id = (string) $model->person_id;
    }
}