<?php
return [
    'log_file'        => null,
    'callback_login'  => null,
    'callback_reject' => function () {
        header('Location: http://coub.com/view/3y4hh');
        exit();
    },
    'server_hostname' => 'taxsee.ru',
    'server_port'     => 443,
    'server_uri'      => 'cas',
    'allowed_clients' => ['10.145.0.42'],
    //массив подстрок которые обязаны быть в ролях пользователя
    'roles_required'  => [],
    //массив подстрок которые обязаны отсутствовать в ролях пользователя
    'roles_disabled'  => [],
];